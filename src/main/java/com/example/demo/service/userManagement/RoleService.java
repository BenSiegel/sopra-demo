package com.example.demo.service.userManagement;

import com.example.demo.entity.userManagement.Rolle;
import com.example.demo.repository.userManagement.RoleRepo;
import org.springframework.stereotype.Service;


@Service
public class RoleService {

  private final RoleRepo roleRepo;


  /**
   * Constructor for spring dependency injection.
   */
  public RoleService(RoleRepo roleRepo) {
    this.roleRepo = roleRepo;
  }

  public Rolle persistRole(Rolle rolle) {
    return roleRepo.save(rolle);
  }

  public Rolle findRoleByName(String rollename) {
    return roleRepo.findByRolleName(rollename);
  }

}
