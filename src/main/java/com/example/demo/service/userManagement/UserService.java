package com.example.demo.service.userManagement;

import com.example.demo.entity.userManagement.Rolle;
import com.example.demo.entity.userManagement.Benutzer;
import com.example.demo.repository.userManagement.UserRepo;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;


@Service
public class UserService implements UserDetailsService {

  private final UserRepo userRepo;

  private final BCryptPasswordEncoder bCryptPasswordEncoder;

  /**
   * Constructor for spring dependency injection.
   */
  public UserService(UserRepo userRepo, BCryptPasswordEncoder bCryptPasswordEncoder) {
    this.userRepo = userRepo;
    this.bCryptPasswordEncoder = bCryptPasswordEncoder;
  }

  /**
   * Saves a user-object and encodes its password.
   *
   * @param benutzer the user to persist.
   * @return the persisted user-object.
   */
  public Benutzer persistUser(Benutzer benutzer) {
    // encode password before saving
    benutzer.setPassword(bCryptPasswordEncoder.encode(benutzer.getPassword()));
    return userRepo.save(benutzer);
  }

  /**
   * Returns the User-object of a given username.
   * @param username the username.
   * @return User-object.
   */
  public Benutzer findUserByUsername(String username) {
    return userRepo.findByUsername(username);
  }

  /**
   * Returns the currently logged in User.
   *
   * @return User.
   */
  public Benutzer getCurrentUser() {
    return findUserByUsername(((org.springframework.security.core.userdetails.User) SecurityContextHolder.getContext()
        .getAuthentication().getPrincipal()).getUsername());
  }

  /**
   * Returns the UserDetails Object of the current logged in user.
   * Might be used if any role-authentication checks have to be performed.
   *
   * @return UserDetails object of Spring Security.
   */
  public org.springframework.security.core.userdetails.User getCurrentUserDetails() {
    return (org.springframework.security.core.userdetails.User) SecurityContextHolder.getContext()
        .getAuthentication().getPrincipal();
  }

  /**
   * Override method, which is required during the login for the Spring Security.
   *
   * @param username the username of the corresponding user.
   * @return UserDetails object of the Spring Security Framework.
   * @throws UsernameNotFoundException exception.
   */
  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    Benutzer benutzer = userRepo.findByUsername(username);
    if (Objects.isNull(benutzer)) {
      throw new UsernameNotFoundException("Could not find the user for username " + username);
    }
    List<GrantedAuthority> grantedAuthorities = getUserAuthorities(benutzer.getRoles());
    return new org.springframework.security.core.userdetails.User(benutzer.getUsername(), benutzer.getPassword(),
        benutzer.isEnabled(), true, true, benutzer.isNonLocked(), grantedAuthorities);
  }

  private List<GrantedAuthority> getUserAuthorities(Set<Rolle> rolleSet) {
    List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
    for (Rolle rolle : rolleSet) {
      grantedAuthorities.add(new SimpleGrantedAuthority(rolle.getRolleName()));
    }
    return grantedAuthorities;
  }
}
