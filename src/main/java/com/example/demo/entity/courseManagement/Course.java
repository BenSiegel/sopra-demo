package com.example.demo.entity.courseManagement;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;

import java.time.Instant;


@Entity
public class Course {

  @Id
  @GeneratedValue
  private Integer id;

  private String name;

  private Instant creationDate;

  /**
   * Empty constructor required for Hibernate.
   */
  public Course() {

  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Instant getCreationDate() {
    return creationDate;
  }

  public void setCreationDate(Instant creationDate) {
    this.creationDate = creationDate;
  }
}
