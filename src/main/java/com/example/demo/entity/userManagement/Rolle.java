package com.example.demo.entity.userManagement;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import org.springframework.security.core.GrantedAuthority;




@Entity
@Table(name = "rolle")
public class Rolle implements GrantedAuthority {

  @Id
  @GeneratedValue
  @Column(name = "rolle_id")
  private Integer id;

  @Column(name = "rolleName")
  private String rolleName;

  @Override
  public String getAuthority(){
    return rolleName;
  }

  ///////////////////////////////////////////////
  // Getter & Setter
  ///////////////////////////////////////////////

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getRolleName() {
    return rolleName;
  }

  public void setRolleName(String rolleName) {
    this.rolleName = rolleName;
  }

}
