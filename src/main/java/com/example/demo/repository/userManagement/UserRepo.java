package com.example.demo.repository.userManagement;

import com.example.demo.entity.userManagement.Benutzer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepo extends JpaRepository<Benutzer, Integer> {

  Benutzer findByUsername(String username);
}
