package com.example.demo.repository.userManagement;

import com.example.demo.entity.userManagement.Rolle;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepo extends JpaRepository<Rolle, Integer> {

  Rolle findByRolleName(String roleName);
}
