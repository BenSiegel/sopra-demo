package com.example.demo.repository.courseManagement;

import com.example.demo.entity.courseManagement.Course;
import org.springframework.data.jpa.repository.JpaRepository;


public interface CourseRepo extends JpaRepository<Course, Integer> {

}
