package com.example.demo.config;

import com.example.demo.entity.courseManagement.Course;
import com.example.demo.entity.userManagement.Rolle;
import com.example.demo.entity.userManagement.Benutzer;
import com.example.demo.service.courseManagement.CourseService;
import com.example.demo.service.userManagement.RoleService;
import com.example.demo.service.userManagement.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * This class is responsible for setting up some test data.
 */
@Component
public class TestDatabaseLoader implements CommandLineRunner {

  @Autowired
  private CourseService courseService;

  @Autowired
  private UserService userService;

  @Autowired
  private RoleService roleService;

  /**
   * Executed during Spring boot startup.
   * @param args arguments.
   * @throws Exception exception.
   */
  @Override
  public void run(String... args) throws Exception {
    // create roles
    Rolle userRolle = new Rolle();
    userRolle.setRolleName("ROLE_USER");
    roleService.persistRole(userRolle);

    Rolle adminRolle = new Rolle();
    adminRolle.setRolleName("ROLE_ADMIN");
    roleService.persistRole(adminRolle);

    Set<Rolle> userRolles = new HashSet<>();
    userRolles.add(userRolle);

    Set<Rolle> adminRolles = new HashSet<>();
    adminRolles.add(adminRolle);

    // create users
    Benutzer admin = new Benutzer();
    admin.setEmail("admin@mail.com");
    admin.setUsername("admin");
    admin.setPassword("admin");
    admin.setEnabled(true);
    admin.setNonLocked(true);
    admin.setRoles(adminRolles);
    userService.persistUser(admin);

    // create course entities
    Course sopra = new Course();
    sopra.setName("Softwarepraktikum");
    sopra.setCreationDate(Instant.now());
    courseService.persistCourse(sopra);

  }
}
